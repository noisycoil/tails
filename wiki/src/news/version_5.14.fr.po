# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-06-13 14:17+0200\n"
"PO-Revision-Date: 2024-09-07 10:07+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta title=\"Tails 5.14\"]]\n"
msgstr "[[!meta title=\"Tails 5.14\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta date=\"Tue, 13 Jun 2023 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 13 Jun 2023 12:34:56 +0000\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"features\">New features</h1>\n"
msgstr "<h1 id=\"features\">Nouvelles fonctionnalités</h1>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "Automatic migration to LUKS2 and Argon2id"
msgstr "Migration automatique vers LUKS2 et Argon2id"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"<p>The cryptographic parameters of LUKS from Tails 5.12 or earlier are\n"
"weak against a state-sponsored attacker with physical access to your\n"
"device.</p>\n"
msgstr ""
"<p>Les paramètres cryptographiques de LUKS de Tails 5.12 ou plus ancien "
"sont\n"
"faibles contre un attaquant appuyé par un État et avec un accès physique à "
"votre\n"
"périphérique.</p>\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, markdown-text
msgid ""
"To use stronger encryption parameters, Tails 5.14 automatically converts "
"your Persistent Storage to use LUKS2 encryption with Argon2id."
msgstr ""
"Pour utiliser des paramètres de chiffrement plus forts, Tails 5.14 convertit "
"automatiquement votre stockage persistant pour utiliser le chiffrement LUKS2 "
"avec Argon2id."

#. type: Plain text
#, markdown-text
msgid ""
"Still, we recommend you change the passphrase of your Persistent Storage and "
"other LUKS encrypted volumes unless you use a long passphrase of 5 random "
"words or more."
msgstr ""
"Néanmoins, nous vous recommandons de changer la phrase de passe de votre "
"stockage persistant et des autres volumes chiffrés LUKS à moins que vous "
"n'utilisiez une longue phrase de passe de 5 mots aléatoires ou plus."

#. type: Plain text
#, markdown-text
msgid "[[Read our security advisory and upgrade guide.|security/argon2id]]"
msgstr ""
"[[Lisez notre bulletin de sécurité et notre guide de mise à jour.|security/"
"argon2id]]"

#. type: Title ##
#, markdown-text, no-wrap
msgid "Full backups from Tails Installer"
msgstr ""

#. type: Plain text
#, markdown-text
msgid ""
"You can now do a backup of your Persistent Storage from *Tails Installer* by "
"cloning your Persistent Storage to your backup Tails entirely."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!img doc/persistent_storage/backup/clone.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img doc/persistent_storage/backup/clone.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
#, markdown-text
msgid ""
"You can still use the backup utility to go faster while [[updating your "
"backup|doc/persistent_storage/backup#updating]]."
msgstr ""

#. type: Title ##
#, markdown-text, no-wrap
msgid "Captive portal detection"
msgstr ""

#. type: Plain text
#, markdown-text
msgid ""
"Tails now detects if you have to sign in to the network using a captive "
"portal if you choose to connect to Tor automatically."
msgstr ""

#. type: Plain text
#, markdown-text
msgid ""
"The error screen appears more quickly and recommends you try to sign in to "
"the network as the first option."
msgstr ""

#. type: Title ##
#, markdown-text, no-wrap
msgid "Incentive to donate from *Electrum*"
msgstr ""

#. type: Plain text
#, markdown-text
msgid ""
"Many people use Tails to secure their Bitcoin wallet and donations in "
"Bitcoin are key to the survival of our project, so we integrated a way to "
"donate from *Electrum* in Tails."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"[[!img electrum.png link=\"no\" alt=\"Popup when starting Electrum with\n"
"button to donate\"]]\n"
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Changements et mises à jour</h1>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "Included software"
msgstr "Logiciels inclus"

#. type: Plain text
#, markdown-text
msgid ""
"- Update *Tor Browser* to "
"[12.0.7](https://blog.torproject.org/new-release-tor-browser-1207)."
msgstr ""
"- Mise à jour du *Navigateur Tor* vers la version [12.0.7](https://blog."
"torproject.org/new-release-tor-browser-1207)."

#. type: Title ##
#, markdown-text, no-wrap
msgid "Usability improvements to the Persistent Storage"
msgstr ""

#. type: Bullet: '- '
#, markdown-text
msgid ""
"Change the button to create a Persistent Storage from the Welcome Screen to "
"be a switch. ([[!tails_ticket 19673]])"
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "  [[!img create.png link=\"no\" alt=\"\"]]\n"
msgstr "  [[!img create.png link=\"no\" alt=\"\"]]\n"

#. type: Bullet: '- '
#, markdown-text
msgid ""
"Add back the description of some of the Persistent Storage features and "
"mention *Kleopatra* in the *GnuPG* feature. ([[!tails_ticket 19642]] and "
"[[!tails_ticket 19675]])"
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "  [[!img descriptions.png link=\"no\" alt=\"\"]]\n"
msgstr "  [[!img descriptions.png link=\"no\" alt=\"\"]]\n"

#. type: Bullet: '- '
#, markdown-text
msgid ""
"Hide the duplicated *Persistent* bookmark in the *Files* browser.  "
"([[!tails_ticket 19646]])"
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Problèmes corrigés</h1>\n"

#. type: Plain text
#, markdown-text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."
msgstr ""
"Pour plus de détails, lisez notre [[!tails_gitweb debian/changelog desc="
"\"liste des changements\"]]."

#. type: Bullet: '- '
#, markdown-text
msgid ""
"Avoid restarting the desktop environment when creating a Persistent "
"Storage. ([[!tails_ticket 19667]])"
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Problèmes connus</h1>\n"

#. type: Plain text
#, markdown-text
msgid "None specific to this release."
msgstr "Aucun spécifique à cette version."

#. type: Plain text
#, markdown-text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""
"Voir la liste des [[problèmes connus de longue date|support/known_issues]]."

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.14</h1>\n"
msgstr "<h1 id=\"get\">Obtenir Tails 5.14</h1>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "To upgrade your Tails USB stick and keep your Persistent Storage"
msgstr ""
"Pour mettre à jour votre clé USB Tails et conserver votre stockage persistant"

#. type: Plain text
#, markdown-text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.14."
msgstr ""
"- Mises à jour automatiques disponibles depuis Tails 5.0 ou plus récent vers "
"la version 5.14."

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Vous pouvez [[réduire la taille du téléchargement|doc/upgrade#reduce]] des "
"futures\n"
"  mises à jours automatiques en effectuant une mise à jour manuelle vers la "
"dernière version.\n"

#. type: Bullet: '- '
#, markdown-text
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual "
"upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si vous ne pouvez pas faire une mise à jour automatique ou si le démarrage "
"de Tails échoue après une mise à jour automatique, merci d'essayer de faire "
"une [[mise à jour manuelle|doc/upgrade/#manual]]."

#. type: Title ##
#, markdown-text, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Pour installer Tails sur une nouvelle clé USB"

#. type: Plain text
#, markdown-text
msgid "Follow our installation instructions:"
msgstr "Suivez nos instructions d'installation :"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installer depuis Windows|install/windows]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installer depuis macOS|install/mac]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installer depuis Linux|install/linux]]"

#. type: Bullet: '  - '
#, markdown-text
msgid ""
"[[Install from Debian or Ubuntu using the command line and "
"GnuPG|install/expert]]"
msgstr ""
"[[Installer depuis Debian ou Ubuntu en utilisant la ligne de commande et "
"GnuPG|install/expert]]"

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be "
"lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Le stockage persistant de la clé USB sera perdu "
"si\n"
"vous faites une installation au lieu d'une mise à jour.</p></div>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "To download only"
msgstr "Pour seulement télécharger"

#. type: Plain text
#, markdown-text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.14 directly:"
msgstr ""
"Si vous n'avez pas besoin d'instructions d'installation ou de mise à jour, "
"vous pouvez télécharger directement Tails 5.14 :"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Pour clés USB (image USB)|install/download]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Pour DVD et machines virtuelles (image ISO)|install/download-iso]]"
