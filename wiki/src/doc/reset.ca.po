# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-12-21 23:10+0000\n"
"PO-Revision-Date: 2023-12-22 01:19+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Uninstalling Tails or resetting a USB stick\"]]\n"
msgstr "[[!meta title=\"Desinstal·lar Tails o restablir un llapis USB\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/reset.intro\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/reset.intro.ca\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
msgid ""
"- [[Instructions for Linux with GNOME: Ubuntu, Debian, Tails, etc.|reset/"
"linux]]"
msgstr ""
"- [[Instruccions per a Linux amb GNOME: Ubuntu, Debian, Tails, etc.|reset/"
"linux]]"

#. type: Plain text
msgid "- [[Instructions for Windows|reset/windows]]"
msgstr "- [[Instruccions per a Windows|reset/windows]]"

#. type: Plain text
msgid "- [[Instructions for macOS|reset/mac]]"
msgstr "- [[Instruccions per a macOS|reset/mac]]"
